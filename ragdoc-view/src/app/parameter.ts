import {TypeRef} from './type-ref';

export class Parameter {
  type: TypeRef;
  name: string;

  static fromJson(json: any): Parameter {
    return {
      type: TypeRef.fromJson(json.t),
      name: json.n,
    };
  }
}

