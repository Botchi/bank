import { Directive, ElementRef, Input, OnChanges } from '@angular/core';

import * as CodeMirror from 'codemirror';

import 'codemirror/mode/clike/clike';
import 'codemirror/addon/selection/active-line';

@Directive({
  selector: '[appEditor]'
})
export class EditorDirective implements OnChanges {
  editor: any;
  @Input() sourceText: string;
  @Input() sourceLine: number;

  constructor(public element: ElementRef) {
    this.editor = new CodeMirror.fromTextArea(element.nativeElement, {
      mode: 'text/x-java',
      theme: 'mbo',
      lineNumbers: true,
      styleActiveLine: true,
      lineWrapping: false,
    });
    this.editor.setSize('100%', '100%');
  }

  ngOnChanges() {
    this.editor.setValue(this.sourceText || 'loading...');
    if (this.sourceLine) {
      var line = this.sourceLine - 1;
      this.editor.scrollIntoView({line: line + 30, ch: 0});
      this.editor.setCursor({line: line, ch: 0});
    }
  }
}
