import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeclaredAtComponent } from './declared-at.component';

describe('DeclaredAtComponent', () => {
  let component: DeclaredAtComponent;
  let fixture: ComponentFixture<DeclaredAtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeclaredAtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeclaredAtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
