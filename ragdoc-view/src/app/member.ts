import {Parameter} from './parameter';
import {Doc} from './doc';
import {TypeRef} from './type-ref';

export class Member {
  name: string;
  type?: TypeRef;
  doc: Doc;
  parameters: Parameter[];
  throws: TypeRef[];

  constructor(name: string, type: TypeRef, doc: Doc, parameters: Parameter[],
      throws: TypeRef[]) {
    this.name = name;
    this.type = type;
    this.doc = doc;
    this.parameters = parameters;
    this.throws = throws;
  }

  get isVoid(): boolean {
    return this.type.isVoid;
  }

  static fromJson(json: any): Member {
    var params: Parameter[] = [];
    var doc: Doc = undefined;
    var name: string = json.name as string;
    if (json.doc) {
      doc = Doc.fromJson(json.doc);
    }
    if (json.params) {
      params = json.params.map(param => Parameter.fromJson(param));
    }
    var throws: TypeRef[] = undefined;
    if (json.throws) {
      throws = (json.throws as TypeRef[]).map(TypeRef.fromJson);
    }
    if (json.type) {
      return new Member(name,
          TypeRef.fromJson(json.type),
          doc,
          params,
          throws);
    } else {
      return new Member(name,
          undefined,
          doc,
          params,
          throws);
    }
  }
}
