import { Component, Input, ComponentFactoryResolver } from '@angular/core';

import {Parameter} from './parameter';

@Component({
  selector: 'parameters',
  styles: [`
    .parameters {
      display: inline;
      height: 1.2em;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    .parameter {
      display: inline;
    }
    .sep {
      display: inline;
      margin-right: .3em;
    }
  `],
  template: `
    <div class="parameters">
      <div *ngFor="let param of params; let isLast=last" class="parameter">
        <type-ref [type]="param.type"></type-ref><div *ngIf="!isLast" class="sep">,</div>
      </div>
    </div>
  `
})
export class ParametersComponent {
  @Input() params : Parameter[];

  constructor(private _componentFactoryResolver: ComponentFactoryResolver) { }
}
