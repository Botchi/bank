import {TypeRef} from './type-ref';

export class InheritedMembers {
  superclass: TypeRef;
  members: string[];

  static fromJson(json: any): InheritedMembers {
    return {
      superclass: TypeRef.fromJson(json.superclass),
      members: json.members as string[],
    };
  }
}
