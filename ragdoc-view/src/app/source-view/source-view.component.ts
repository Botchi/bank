import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import 'rxjs/add/operator/switchMap';

import {SourceService} from '../source.service';

@Component({
  selector: 'app-source-viewer',
  providers: [ SourceService ],
  template: `<textarea appEditor [sourceText]="source" [sourceLine]="line">{{source}}</textarea>`,
})
export class SourceViewComponent implements OnInit {
  source: string;
  line: number;

  constructor(private sourceService: SourceService,
      private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.switchMap((params: Params) => {
      this.line = +params['line'];
      return this.sourceService.getSource(params['filename']);
    })
    .subscribe(source => {
      this.source = source;
    });
  }

}
