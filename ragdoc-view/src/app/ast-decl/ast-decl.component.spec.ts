import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AstDeclComponent } from './ast-decl.component';

describe('AstDeclComponent', () => {
  let component: AstDeclComponent;
  let fixture: ComponentFixture<AstDeclComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AstDeclComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AstDeclComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
