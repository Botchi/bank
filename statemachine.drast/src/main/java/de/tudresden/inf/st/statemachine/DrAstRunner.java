package de.tudresden.inf.st.statemachine;

import drast.model.DrASTSettings;
import drast.views.gui.DrASTGUI;

/**
 * Extended runner for DrAST
 *
 * @author jmey - Initial contribution
 */
public class DrAstRunner {

  public static void main(String[] args) {
    DrASTSettings.put(DrASTSettings.PREV_JAR, "../statemachine.solution/build/libs/statemachine.solution-0.1.jar");
    DrASTSettings.put(DrASTSettings.PREV_FIRST_ARG, "../statemachine.solution/src/test/resources/empty.sm");
    DrASTGUI.main(args);
  }

}
