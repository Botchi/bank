package de.tudresden.inf.st.bank;

import beaver.Parser;
//import de.tudresden.inf.st.bank.jastadd.model.Bank;
import de.tudresden.inf.st.bank.jastadd.model.RoleRag;
import de.tudresden.inf.st.bank.jastadd.parser.BankParser;
import de.tudresden.inf.st.bank.jastadd.scanner.BankScanner;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Utility methods for loading statemachine files.
 *
 * @author rschoene - Initial contribution
 */
public class ParserUtils {
  static RoleRag load(Path path) throws IOException, Parser.Exception {
    Reader reader = Files.newBufferedReader(path);
    BankScanner scanner = new BankScanner(reader);
    BankParser parser = new BankParser();
    RoleRag result = (RoleRag) parser.parse(scanner);
    reader.close();
    return result;
  }
}
