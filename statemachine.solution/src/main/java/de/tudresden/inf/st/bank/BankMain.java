package de.tudresden.inf.st.bank;

import beaver.Parser;
//import de.tudresden.inf.st.statemachine.jastadd.model.State;
//import de.tudresden.inf.st.statemachine.jastadd.model.StateMachine;
//import de.tudresden.inf.st.statemachine.jastadd.model.Transition;
import de.tudresden.inf.st.bank.jastadd.model.*;

import java.io.IOException;
import java.nio.file.Paths;

public class BankMain {

  @SuppressWarnings("WeakerAccess")
  public static Object DrAST_root_node;

  public static void main(String[] args) throws IOException, Parser.Exception {
    //Bank bank;
    if (args.length == 0) {
      //bank = createExample();
      createExample();
    } else {
      // load the file given as first argument
      //bank = ParserUtils.load(Paths.get(args[0]));
    }

    //System.out.println("Bankname: " + bank.getName() + " " + bank.getName2());
  }

  private static void createExample() {
    RoleRag rag = new RoleRag();
    //Consultant con2 = rag.CreateConsultant();
    /*
    Person peter = new Person();
    peter.setName("Peter");
    Consultant con = new Consultant();
    con.setName("Con");
    Customer cu = new Customer();
    cu.setName("Cu");

    //Fulfillment
    //PersonConsultantFulfillment f1 = new PersonConsultantFulfillment();
    //f1.setFiller(peter);
    //f1.setFilled(con);

    // Relationship advices: Consultant advices Customer
    Relationship advices = new Relationship();
    advices.setName("advices");
    advices.setFirst(con);
    advices.setSecond(cu);
    con.addOutgoing(advices);
    cu.addIncoming(advices);
    */
    // Irreflexive for advices: Consultant --- Customer
    //Irreflexive irreflexive = new Irreflexive();
    //irreflexive.setRelation(advices);

    Attribute phoneNumber1 = new Attribute();
    phoneNumber1.setName("PhoneNumber");
    phoneNumber1.setValue("123");
    Attribute phoneNumber2 = new Attribute();
    phoneNumber2.setName("PhoneNumber");
    phoneNumber2.setValue("234");
    Attribute phoneNumber3 = new Attribute();
    phoneNumber3.setName("PhoneNumber");
    phoneNumber3.setValue("345");

    Attribute addresse2 = new Attribute();
    addresse2.setName("Addresse");
    addresse2.setValue("bcd");

    /*
    peter.addAttribute(phoneNumber1);
    //Peter.addOriginalAttributes(phoneNumber1);
    con.addAttribute(phoneNumber2);
    cu.addAttribute(phoneNumber3);
    con.addAttribute(addresse2);

    //output
    //System.out.println(Peter.getChild(0).getChild(0).dumpTree());
    //System.out.println("Peter.PNr " + Peter.getAttributeByName("PhoneNumber"));
    //System.out.println(peter.dumpTree());

    System.out.println("peter.PhoneNumber: " + peter.PhoneNumber().getValue());

    peter.plays(con);
    System.out.println("peter.plays(Con)");
    //System.out.println("Peter.plays(Con) -> Peter.PNr " + Peter.getAttributeByName("PhoneNumber"));
    //System.out.println(Peter.dumpTree());

    //System.out.println(peter.roles().get(0).dumpTree());
    //peter.roles().forEach(role -> System.out.println(role.dumpTree()));
    System.out.println("peter.PhoneNumber: " + peter.PhoneNumber().getValue());

    peter.plays(cu);
    System.out.println("peter.plays(Cu)");
    //System.out.println("Peter.plays(Cu) -> Peter.PNr " + Peter.getAttributeByName("PhoneNumber"));
    //System.out.println(Peter.dumpTree());
    System.out.println("peter.PhoneNumber: " + peter.PhoneNumber().getValue());

    peter.unplays(con);
    System.out.println("peter.unplays(Con)");
    //System.out.println("Peter.unplays(Con) -> Peter.PNr " + Peter.getAttributeByName("PhoneNumber"));
    //System.out.println(Peter.dumpTree());

    //peter.roles().forEach(role -> System.out.println(role.dumpTree()));
    System.out.println("peter.PhoneNumber: " + peter.PhoneNumber().getValue());

    peter.unplays(cu);
    System.out.println("peter.unplays(Cu)");
    //System.out.println("Peter.unplays(Cu) -> Peter.PNr " + Peter.getAttributeByName("PhoneNumber"));
    //System.out.println(Peter.dumpTree());
    System.out.println("peter.PhoneNumber: " + peter.PhoneNumber().getValue());

    //Bank b = new Bank();
    //rag.addCompartmentType(b);
    */
    //System.out.println("new");
    Bank b2 = rag.CreateBank("Bank");
    Consultant con2 = rag.CreateConsultant("Con2");
    Customer cus2 = rag.CreateCustomer("Cus2");
    Person peter2 = rag.CreatePerson("Peter2");

    peter2.addAttribute(phoneNumber1);
    con2.addAttribute(phoneNumber2);
    //con2.advises(cus2);

    System.out.println(rag.prettyPrint());

    //System.out.println("peter2.PhoneNumber: " + peter2.PhoneNumber().getValue());
    System.out.println("peter2.plays(con2, b2)");
    peter2.plays(con2, b2);
    con2.advises(cus2);
    //System.out.println("peter2.PhoneNumber: " + peter2.PhoneNumber().getValue());

    System.out.println(rag.prettyPrint());
  }

}
