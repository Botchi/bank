package de.tudresden.inf.st.bank.jastadd.scanner;

import de.tudresden.inf.st.bank.jastadd.parser.BankParser.Terminals; // The terminals are implicitly defined in the parser
%%
// Documentation links: https://www.jflex.de/manual.html and http://beaver.sourceforge.net/scanners.html

// define the signature for the generated scanner
%public
%final
%class BankScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r | \r\n
Identifier = [:jletter:][:jletterdigit:]*

%%

// discard whitespace information
{WhiteSpace}  { }

// token definitions
"naturaltype" { return sym(Terminals.NATURALTYPE); }
//"roletype"    { return sym(Terminals.ROLETYPE); }
//"fulfillment" { return sym(Terminals.FULFILLMENT); }
{Identifier}  { return sym(Terminals.NAME); }
//"::="         { return sym(Terminals.ASSIGN); }
<<EOF>>       { return sym(Terminals.EOF); }
