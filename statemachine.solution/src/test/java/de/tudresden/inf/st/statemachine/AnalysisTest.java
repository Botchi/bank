package de.tudresden.inf.st.statemachine;

import beaver.Parser;
import de.tudresden.inf.st.bank.ParserUtils;
//import de.tudresden.inf.st.statemachine.jastadd.model.State;
//import de.tudresden.inf.st.statemachine.jastadd.model.StateMachine;
//import de.tudresden.inf.st.statemachine.jastadd.model.Transition;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Testing analysis attributes.
 *
 * @author rschoene - Initial contribution
 */
public class AnalysisTest {
/*
  @Test
  public void test1() throws IOException, Parser.Exception {
    StateMachine stateMachine = ParserUtils.load(Paths.get("src", "test", "resources", "machine_one.sm"));
    State s = stateMachine.globallyResolveStateByToken("S");
    State a = stateMachine.globallyResolveStateByToken("A");
    State e = stateMachine.globallyResolveStateByToken("E");

    assertEquals(0, s.minDistTo(s));
    assertEquals(1, s.minDistTo(a));
    assertEquals(2, s.minDistTo(e));
    assertEquals(1, a.minDistTo(e));
    assertEquals(1, a.minDistTo(s));
    assertEquals(0, a.minDistTo(a));
    assertEquals(-1, e.minDistTo(s));
    assertEquals(-1, e.minDistTo(a));
    assertEquals(0, e.minDistTo(e));
  }

  @Test
  public void testEmpty() throws IOException, Parser.Exception {
    StateMachine stateMachine = ParserUtils.load(Paths.get("src", "test", "resources", "empty.sm"));

    State s = stateMachine.globallyResolveStateByToken("S");
    State a = stateMachine.globallyResolveStateByToken("A");
    State e = stateMachine.globallyResolveStateByToken("E");

    assertEquals(0, s.minDistTo(s));
    assertEquals(1, s.minDistTo(a));
    assertEquals(2, s.minDistTo(e));
    assertEquals(1, a.minDistTo(e));
    assertEquals(1, a.minDistTo(s));
    assertEquals(0, a.minDistTo(a));
    assertEquals(-1, e.minDistTo(s));
    assertEquals(-1, e.minDistTo(a));
    assertEquals(0, e.minDistTo(e));

    // change some things
    Transition t5 = new Transition();
    t5.setFrom(a);
    t5.setTo(e);
    stateMachine.addElement(t5);

    System.out.println(stateMachine.prettyPrint());

    assertEquals(0, s.minDistTo(s));
    assertEquals(1, s.minDistTo(a));
    assertEquals(1, s.minDistTo(e));
    assertEquals(0, a.minDistTo(e));
    assertEquals(1, a.minDistTo(s));
    assertEquals(0, a.minDistTo(a));
    assertEquals(-1, e.minDistTo(s));
    assertEquals(-1, e.minDistTo(a));
    assertEquals(0, e.minDistTo(e));

    Set<Transition> epsilons = stateMachine.epsilonTransitions();
    assertEquals(2, epsilons.size());

    for (Transition eps : epsilons) {
      removeEpsilonTransition(stateMachine, eps);
    }

    State ae = stateMachine.globallyResolveStateByToken("A+E");
    Transition t6 = new Transition();
    t6.setFrom(s);
    t6.setTo(ae);
    stateMachine.addElement(t6);

    System.out.println(stateMachine.prettyPrint());

    epsilons = stateMachine.epsilonTransitions();
    assertEquals(1, epsilons.size());

    for (Transition eps : epsilons) {
      removeEpsilonTransition(stateMachine, eps);
    }
  }

  private void removeEpsilonTransition(StateMachine stateMachine, Transition eps) {
    System.out.print("removing epsilon transition " + eps.prettyPrint());
    System.out.println("Minimal distances before:");
    initialToFinalDists(stateMachine);
    stateMachine.removeEpsilonTransition(eps);
    System.out.println("Minimal distances after:");
    initialToFinalDists(stateMachine);
    System.out.println("StateMachine after");
    System.out.println(stateMachine.prettyPrint());
  }

  private void initialToFinalDists(StateMachine stateMachine) {
    for (State finalState : stateMachine.getFinalList()) {
      System.out.println("initial state "+ stateMachine.getInitial() + " to " + finalState + " in " +
          stateMachine.getInitial().minDistTo(finalState) + " step(s)");
    }
  }*/
}
