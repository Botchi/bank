package de.tudresden.inf.st.statemachine;

import beaver.Parser;
import de.tudresden.inf.st.jastadd.dumpAst.ast.Dumper;
import de.tudresden.inf.st.statemachine.jastadd.model.State;
import de.tudresden.inf.st.statemachine.jastadd.model.StateMachine;
import de.tudresden.inf.st.statemachine.jastadd.model.Transition;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Set;

public class Main {

  @SuppressWarnings("WeakerAccess")
  public static Object DrAST_root_node;

  public static void main(String[] args) throws IOException, Parser.Exception {
    StateMachine stateMachine;
    if (args.length == 0) {
      stateMachine = createExample();
    } else {
      // load the file given as first argument
      stateMachine = ParserUtils.load(Paths.get(args[0]));
    }
    printHeading("Initial statemachine");
    System.out.println(stateMachine.prettyPrint());
    Dumper.read(stateMachine).dumpAsPNG(Paths.get("01-initial.png"));

    stateMachine.printSomeAnalysis();

    Set<Transition> epsilons = stateMachine.epsilonTransitions();

    for (Transition eps : epsilons) {
      printHeading("Removing epsilon transition " + eps.prettyPrint().trim());
      System.out.println("Minimal distances before:");
      initialToFinalDistances(stateMachine);
      stateMachine.removeEpsilonTransition(eps);
      System.out.println("Minimal distances after:");
      initialToFinalDistances(stateMachine);
      printHeading("StateMachine after");
      System.out.println(stateMachine.prettyPrint());
    }
    printHeading("DotGraph");
    System.out.println(stateMachine.toDot());
    Dumper.read(stateMachine).dumpAsPNG(Paths.get("02-transformed.png"));
    DrAST_root_node = stateMachine;
  }

  private static void printHeading(String s) {
    System.out.println();
    System.out.println("========================================");
    System.out.println("== " + s);
    System.out.println("========================================");
  }

  private static StateMachine createExample() {
    // manual construction of a simple statemachine
    // (S) -- e --> (B) -- 1 --> (E)
    //  ^            |
    //  \            /
    //  `---- 2 ----*
    StateMachine stateMachine = new StateMachine();
    State start = new State();
    start.setLabel("S");
    State stateB = new State();
    stateB.setLabel("B");
    State end = new State();
    end.setLabel("E");
    Transition eps = new Transition();
    Transition t2 = new Transition();
    t2.setLabel("2");
    Transition t1 = new Transition();
    t1.setLabel("1");
    eps.setFrom(start);
    eps.setTo(stateB);
    t2.setFrom(stateB);
    t2.setTo(start);
    t1.setFrom(stateB);
    t1.setTo(end);
    stateMachine.addElement(start);
    stateMachine.addElement(stateB);
    stateMachine.addElement(end);
    stateMachine.addElement(eps);
    stateMachine.addElement(t2);
    stateMachine.addElement(t1);
    stateMachine.setInitial(start);
    stateMachine.addFinal(end);
    return stateMachine;
  }

  private static void initialToFinalDistances(StateMachine stateMachine) {
    for (State finalState : stateMachine.getFinalList()) {
      System.out.println("initial state "+ stateMachine.getInitial() + " to " + finalState + " in " +
          stateMachine.getInitial().minDistTo(finalState) + " step(s)");
    }
  }

}
