package de.tudresden.inf.st.statemachine;

import beaver.Parser;
import de.tudresden.inf.st.statemachine.jastadd.model.State;
import de.tudresden.inf.st.statemachine.jastadd.model.StateMachine;
import de.tudresden.inf.st.statemachine.jastadd.model.Transition;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.*;

/**
 * Testing the statemachine parser.
 *
 * @author rschoene - Initial contribution
 */
public class ParserTest {

  @Test
  public void test1() throws IOException, Parser.Exception {
    StateMachine stateMachine = ParserUtils.load(Paths.get("src", "test", "resources", "machine_one.sm"));

    assertEquals(6, stateMachine.getNumElement());
    assertEquals(3, stateMachine.states().size());
    assertEquals(3, stateMachine.transitions().size());

    State s = stateMachine.globallyResolveStateByToken("S");
    State a = stateMachine.globallyResolveStateByToken("A");
    State e = stateMachine.globallyResolveStateByToken("E");
    Transition t1 = stateMachine.globallyResolveTransitionByToken("t1");
    Transition t2 = stateMachine.globallyResolveTransitionByToken("t2");
    Transition t3 = stateMachine.globallyResolveTransitionByToken("t3");

    assertNotNull(s);
    assertNotNull(a);
    assertNotNull(e);

    assertEquals(s, stateMachine.getInitial());
    assertEquals(1, stateMachine.getFinalList().size());
    assertThat("E is not final", stateMachine.getFinalList(), hasItem(e));

    assertNotNull(t1);
    assertNotNull(t2);
    assertNotNull(t3);

    // t1: s -> a
    assertEquals(s, t1.getFrom());
    assertEquals(a, t1.getTo());
    // t2: a -> s
    assertEquals(a, t2.getFrom());
    assertEquals(s, t2.getTo());
    // t3: a -> e
    assertEquals(a, t3.getFrom());
    assertEquals(e, t3.getTo());

    List<State> outgoingFromS = outgoingConnectedStates(s);
    List<State> outgoingFromA = outgoingConnectedStates(a);
    assertThat(outgoingFromS, not(empty()));
    assertThat(outgoingFromA, not(empty()));
    assertEquals(1, outgoingFromS.size());
    assertThat("Transition S -> A missing", outgoingFromS, hasItem(a));
    assertEquals(2, outgoingFromA.size());
    assertThat("Transitions A -> S or A -> E missing", outgoingFromA, hasItems(s, e));
    assertThat(outgoingConnectedStates(e), empty());

    String printedForm = "initial state S;\n" +
        "state A;\n" +
        "final state E;\n" +
        "trans S -> A : t1;\n" +
        "trans A -> S : t2;\n" +
        "trans A -> E : t3;\n";
    assertEquals(printedForm, stateMachine.prettyPrint());
  }

  private List<State> outgoingConnectedStates(State s) {
    return s.getOutgoingList().stream().map(Transition::getTo).collect(Collectors.toList());
  }

}
