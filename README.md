# Relational RAGs Example and Exercise

This repository contains an example for the usage of [*Relational RAGs*](https://git-st.inf.tu-dresden.de/johannes.mey/relast).

For basic information on relational RAGs, read
- [Mey et. al., "Continuous model validation using reference attribute grammars", 2018](https://doi.org/10.1145/3276604.3276616)

## Running the example with gradle

:warning: Unfortunately, the gradle build system does not have much backwards-compatibility. Therefore, a suitable version of gradle is bundled with with package that can be run in the main directory with

- using Windows `gradlew.bat <task>`
- using Mac/Linux `./gradlew <task>`

This gradle project uses subprojects, the which themselves contain a number of executable tasks

- `statemachine.solution`
    - this subproject contains a complete running example for a very small state machine grammar
    - to see all available gradle tasks, run `./gradlew :statemachine.solution:tasks --all`
    - to run the program, run `./gradlew :statemachine.solution:run`

- `statemachine.task`
    - this subproject contains the same example, but with some omissions to be completed as an exercise
    - to see all available gradle tasks, run `./gradlew :statemachine.task:tasks --all`
    - to run the program, run `./gradlew :statemachine.task:run`
- `statemachine.drast`
    - this project contains a version of the AST debugger DrAST.
    - to run it, use `./gradlew :statemachine.drast:run`
    - Note, that this requires Java 8 and the corresponding JavaFX 8
    - For more information, visit the [DrAST project homepage](https://bitbucket.org/jastadd/drast/)
